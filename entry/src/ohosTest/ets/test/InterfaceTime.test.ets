/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, expect, it, TestType } from '@ohos/hypium'
import { contact, StringBuilder, VCardBuilder, VCardConfig,VCardParserImpl_V21,VCardParser_V21,HOSBase64,
  VCardProperty,VCardEntry} from '@ohos/vcard'
import {VCardUtils} from '@ohos/vcard/src/main/ets/components/utils/VCardUtils'

export default function InterfaceTime() {
  describe("InterfaceTime", () => {

    const BASE_COUNT = 2000
    const BASELINE_CREATEHTTP = 2000

    it("stringBuilder.clear", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        let stringBuilder = new StringBuilder()
        stringBuilder.clear()
      }
      let endTime = new Date().getTime()
      console.log("stringBuilder.clear:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("stringBuilder.clear:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("stringBuilder.size", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        let stringBuilder = new StringBuilder()
        stringBuilder.size()
      }
      let endTime = new Date().getTime()
      console.log("stringBuilder.size:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("stringBuilder.size:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("stringBuilder.toString", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        let stringBuilder = new StringBuilder()
        stringBuilder.toString()
      }
      let endTime = new Date().getTime()
      console.log("stringBuilder.toString:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("stringBuilder.toString:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("stringBuilder.append", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        let stringBuilder = new StringBuilder()
        stringBuilder.append('abc')
      }
      let endTime = new Date().getTime()
      console.log("stringBuilder.append:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("stringBuilder.append:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendNameProperties", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendNameProperties(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendNameProperties:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendNameProperties:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendNickNames", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendNickNames(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendNickNames:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendNickNames:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendPhones", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendPhones(contactall,null)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendPhones:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendPhones:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendNotes", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendNotes(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendNotes:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendNotes:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendIms", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendIms(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendIms:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendIms:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendPostals", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendPostals(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendPostals:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendPostals:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendOrganizations", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendOrganizations(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendOrganizations:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendOrganizations:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendWebsites", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendWebsites(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendWebsites:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendWebsites:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendEvents", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendEvents(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendEvents:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendEvents:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendRelation", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendRelation(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendRelation:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendRelation:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardBuilder.appendGroupMemberShip", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardBuilder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8")
      let contactall = new contact.Contact()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardBuilder.appendGroupMemberShip(contactall)
      }
      let endTime = new Date().getTime()
      console.log("vCardBuilder.appendGroupMemberShip:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardBuilder.appendGroupMemberShip:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardParserImpl_V21.cancel", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardParserImpl_V21 = new VCardParserImpl_V21()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardParserImpl_V21.cancel()
      }
      let endTime = new Date().getTime()
      console.log("vCardParserImpl_V21.cancel:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardParserImpl_V21.cancel:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("hOSBase64.encode", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let hOSBase64 = new HOSBase64()
      for (let index = 0; index < BASE_COUNT; index++) {
        hOSBase64.encode('a')
      }
      let endTime = new Date().getTime()
      console.log("hOSBase64.encode:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("hOSBase64.encode:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("hOSBase64.decode", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let hOSBase64 = new HOSBase64()
      for (let index = 0; index < BASE_COUNT; index++) {
        hOSBase64.decode('a')
      }
      let endTime = new Date().getTime()
      console.log("hOSBase64.decode:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("hOSBase64.decode:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardProperty.getParamMapString", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardProperty = new VCardProperty()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardProperty.getParamMapString()
      }
      let endTime = new Date().getTime()
      console.log("vCardProperty.getParamMapString:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardProperty.getParamMapString:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardProperty.setName", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardProperty = new VCardProperty()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardProperty.setName('')
      }
      let endTime = new Date().getTime()
      console.log("vCardProperty.setName:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardProperty.setName:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardProperty.addGroup", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardProperty = new VCardProperty()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardProperty.addGroup('')
      }
      let endTime = new Date().getTime()
      console.log("vCardProperty.addGroup:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardProperty.addGroup:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getNameData", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getNameData()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getNameData:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getNameData:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getNickNameList", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getNickNameList()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getNickNameList:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getNickNameList:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getBirthday", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getBirthday()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getBirthday:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getBirthday:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getPhoneList", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getPhoneList()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getPhoneList:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getPhoneList:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getEmailList", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getEmailList()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getEmailList:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getEmailList:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getPostalList", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getPostalList()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getPostalList:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getPostalList:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("vCardEntry.getOrganizationList", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      let vCardEntry = new VCardEntry()
      for (let index = 0; index < BASE_COUNT; index++) {
        vCardEntry.getOrganizationList()
      }
      let endTime = new Date().getTime()
      console.log("vCardEntry.getOrganizationList:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("vCardEntry.getOrganizationList:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.isV21Word", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.isV21Word('abc')
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.isV21Word:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.isV21Word:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.containsOnlyPrintableAscii", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.containsOnlyPrintableAscii("\r\n")
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.containsOnlyPrintableAscii:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.containsOnlyPrintableAscii:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.toStringAsV30ParamValue", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.toStringAsV30ParamValue("HOME")
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.toStringAsV30ParamValue:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.toStringAsV30ParamValue:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.appearsLikeVCardQuotedPrintable", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.appearsLikeVCardQuotedPrintable("=")
      }
      let endTime = new Date().getTime()
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.toHalfWidthString", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.toHalfWidthString(null)
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.toHalfWidthString:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.toHalfWidthString:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.toStringAsParamValue", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.toStringAsParamValue('',[1,2,3])
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.toStringAsParamValue:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.toStringAsParamValue:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.toStringAsV40ParamValue", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.toStringAsV40ParamValue('')
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.toStringAsV40ParamValue:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.toStringAsV40ParamValue:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.areAllEmpty", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.areAllEmpty('')
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.areAllEmpty:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.areAllEmpty:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
    it("VCardUtils.getPhoneNumberFormat", TestType.PERFORMANCE, async (done: Function) => {
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        VCardUtils.getPhoneNumberFormat(1)
      }
      let endTime = new Date().getTime()
      console.log("VCardUtils.getPhoneNumberFormat:" + endTime);
      let averageTime = ((endTime - startTime) * 1000) / BASE_COUNT
      console.log("VCardUtils.getPhoneNumberFormat:" + averageTime + "μs");
      expect(averageTime < BASELINE_CREATEHTTP).assertTrue();
      done()
    });
  });
}