/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { InputStream } from "./JType";
import { VCardConstants } from "./VCardConstants";
import { VCardInterpreter } from "./VCardInterpreter";
import { VCardParser } from "./VCardParser";
import { VCardParserImpl_V30 } from "./VCardParserImpl_V30";
/**
 * </p>
 * vCard parser for vCard 2.1. See the specification for more detail about the spec itself.
 * </p>
 * <p>
 * The spec is written in 1996, and currently various types of "vCard 2.1" exist.
 * To handle real the world vCard formats appropriately and effectively, this class does not
 * obey with strict vCard 2.1.
 * In stead, not only vCard spec but also real world vCard is considered.
 * </p>
 * e.g. A lot of devices and software's let vCard importer/exporter to use
 * the PNG format to determine the type of image, while it is not allowed in
 * the original specification. As of 2010, we can see even the FLV format
 * (possible in Japanese mobile phones).
 * </p>
 */
type v30Property = 
    "BEGIN" | "END" | "LOGO" | "PHOTO" | "LABEL" | "FN" | "TITLE" | "SOUND" |
    "VERSION" | "TEL" | "EMAIL" | "TZ" | "GEO" | "NOTE" | "URL" |
    "BDAY" | "ROLE" | "REV" | "UID" | "KEY" | "MAILER" | "NAME" | 
    "PROFILE"| "SOURCE"| "NICKNAME"| "CLASS"|
    "SORT-STRING"| "CATEGORIES"| "PRODID"| "IMPP";

type V30Type = "DOM" | "INTL" | "POSTAL" | "PARCEL" | "HOME" | "WORK" |
    "PREF" | "VOICE" | "FAX" | "MSG" | "CELL" | "PAGER" | "BBS" |
    "MODEM" | "CAR" | "ISDN" | "VIDEO" | "AOL" | "APPLELINK" |
    "ATTMAIL" | "CIS" | "EWORLD" | "INTERNET" | "IBMMAIL" |
    "MCIMAIL" | "POWERSHARE" | "PRODIGY" | "TLX" | "X400" | "GIF" |
    "CGM" | "WMF" | "BMP" | "MET" | "PMB" | "DIB" | "PICT" | "TIFF" |
    "PDF" | "PS" | "JPEG" | "QTIME" | "MPEG" | "MPEG2" | "AVI" |
    "WAVE" | "AIFF" | "PCM" | "X509" | "PGP";

type V30Value = "INLINE" | "URL" | "CONTENT-ID" | "CID"

export class VCardParser_V30 extends VCardParser {
    /**
     * A unmodifiable Set storing the property names available in the vCard 2.1 specification.
     */
    /* package */ static sKnownPropertyNameSet: Set<v30Property> =
        new Set<v30Property>([
            "BEGIN", "END", "LOGO", "PHOTO", "LABEL", "FN", "TITLE", "SOUND",
                    "VERSION", "TEL", "EMAIL", "TZ", "GEO", "NOTE", "URL",
                    "BDAY", "ROLE", "REV", "UID", "KEY", "MAILER", // 2.1
                    "NAME", "PROFILE", "SOURCE", "NICKNAME", "CLASS",
                    "SORT-STRING", "CATEGORIES", "PRODID",  // 3.0
                    "IMPP"
        ]);


    /**
     * A unmodifiable Set storing the types known in vCard 2.1.
     */
    /* package */ public static readonly sKnownTypeSet =
        new Set<V30Type>([
            "DOM", "INTL", "POSTAL", "PARCEL", "HOME", "WORK",
            "PREF", "VOICE", "FAX", "MSG", "CELL", "PAGER", "BBS",
            "MODEM", "CAR", "ISDN", "VIDEO", "AOL", "APPLELINK",
            "ATTMAIL", "CIS", "EWORLD", "INTERNET", "IBMMAIL",
            "MCIMAIL", "POWERSHARE", "PRODIGY", "TLX", "X400", "GIF",
            "CGM", "WMF", "BMP", "MET", "PMB", "DIB", "PICT", "TIFF",
            "PDF", "PS", "JPEG", "QTIME", "MPEG", "MPEG2", "AVI",
            "WAVE", "AIFF", "PCM", "X509", "PGP"]);

    /**
     * A unmodifiable Set storing the values for the type "VALUE", available in the vCard 2.1.
     */
    /* package */ static readonly sKnownValueSet = new Set<V30Value>(["INLINE", "URL", "CONTENT-ID", "CID"]);

    /**
     * <p>
     * A unmodifiable Set storing the values for the type "ENCODING", available in the vCard 2.1.
     * </p>
     * <p>
     * Though vCard 2.1 specification does not allow "B" encoding, some data may have it.
     * We allow it for safety.
     * </p>
     */
    /* package */ public static readonly sAvailableEncoding =
        new Set<string>(
            [VCardConstants.PARAM_ENCODING_7BIT,
            VCardConstants.PARAM_ENCODING_8BIT,
            VCardConstants.PARAM_ENCODING_QP,
            VCardConstants.PARAM_ENCODING_BASE64,
            VCardConstants.PARAM_ENCODING_B]);

    private mVCardParserImpl: VCardParserImpl_V30;

    constructor();

    constructor(vcardType?: number) {
        super()
        if (vcardType != null) {
            this.mVCardParserImpl = new VCardParserImpl_V30(vcardType);
        } else {
            this.mVCardParserImpl = new VCardParserImpl_V30();
        }
    }

    public addInterpreter(interpreter: VCardInterpreter): void {
        this.mVCardParserImpl.addInterpreter(interpreter);
    }

    public parse(is: InputStream): void {
        this.mVCardParserImpl.parse(is);
    }

    public parseOne(is: InputStream): void {
        this.mVCardParserImpl.parseOne(is);
    }

    public cancel(): void {
        this.mVCardParserImpl.cancel();
    }
}
