/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class VCardConstants{
    /* package */ public static readonly LOG_TAG = "vCard";
    public static readonly VERSION_V21 = "2.1";
    public static readonly VERSION_V30 = "3.0";
    public static readonly VERSION_V40 = "4.0";

    // Valid and supported property names.
    public static readonly PROPERTY_BEGIN = "BEGIN";
    public static readonly PROPERTY_VERSION = "VERSION";
    public static readonly PROPERTY_N = "N";
    public static readonly PROPERTY_FN = "FN";
    public static readonly PROPERTY_ADR = "ADR";
    public static readonly PROPERTY_EMAIL = "EMAIL";
    public static readonly PROPERTY_NOTE = "NOTE";
    public static readonly PROPERTY_ORG = "ORG";
    public static readonly PROPERTY_SOUND = "SOUND";  // Not fully supported.
    public static readonly PROPERTY_TEL = "TEL";
    public static readonly PROPERTY_TITLE = "TITLE";
    public static readonly PROPERTY_ROLE = "ROLE";
    public static readonly PROPERTY_PHOTO = "PHOTO";
    public static readonly PROPERTY_LOGO = "LOGO";
    public static readonly PROPERTY_URL = "URL";
    public static readonly PROPERTY_BDAY = "BDAY";  // Birthday (3.0, 4.0)
    public static readonly PROPERTY_ANNIVERSARY = "ANNIVERSARY";  // Date of marriage (4.0)
    public static readonly PROPERTY_NAME = "NAME";  // (3.0)
    public static readonly PROPERTY_NICKNAME = "NICKNAME";  // (3.0, 4.0)
    public static readonly PROPERTY_SORT_STRING = "SORT-STRING";  // (3.0, 4.0)
    public static readonly PROPERTY_IMPP = "IMPP";  // RFC 4770 (vCard 3.0) and vCard 4.0
    public static readonly PROPERTY_END = "END";

    // Valid property names which are not supported (not appropriately handled) by
    // importer/exporter. Those properties will be read and discarded silently.
    public static readonly PROPERTY_REV = "REV";
    public static readonly PROPERTY_AGENT = "AGENT";  // (3.0)
    public static readonly PROPERTY_GENDER = "GENDER";  // (4.0)
    public static readonly PROPERTY_XML = "XML";  // (4.0)
    public static readonly PROPERTY_FBURL = "FBURL";  // (4.0)
    public static readonly PROPERTY_PRODID = "PRODID";  // (4.0)
    public static readonly PROPERTY_RELATED = "RELATED";  // (4.0)
    public static readonly PROPERTY_CATEGORIES = "CATEGORIES";  // (4.0)
    public static readonly PROPERTY_CLIENTPIDMAP = "CLIENTPIDMAP";  // (4.0)
    public static readonly PROPERTY_CALURI = "CALURI";  // (4.0)
    public static readonly PROPERTY_X_GROUP = "X-GROUP-MEMBERSHIP";

    // defect SIP property which had been used till RFC 4770.
    public static readonly PROPERTY_X_SIP = "X-SIP";

    // Available in vCard 3.0. Should not use when composing vCard 2.1 file.

    // De-fact property values expressing phonetic names.
    public static readonly PROPERTY_X_PHONETIC_FIRST_NAME = "X-PHONETIC-FIRST-NAME";
    public static readonly PROPERTY_X_PHONETIC_MIDDLE_NAME = "X-PHONETIC-MIDDLE-NAME";
    public static readonly PROPERTY_X_PHONETIC_LAST_NAME = "X-PHONETIC-LAST-NAME";

    // Properties both ContactsStruct and de-fact vCard extensions
    // Shown in http://en.wikipedia.org/wiki/VCard support are defined here.
    public static readonly PROPERTY_X_AIM = "X-AIM";
    public static readonly PROPERTY_X_MSN = "X-MSN";
    public static readonly PROPERTY_X_YAHOO = "X-YAHOO";
    public static readonly PROPERTY_X_ICQ = "X-ICQ";
    public static readonly PROPERTY_X_JABBER = "X-JABBER";

    public static readonly PROPERTY_X_SKYPE_USERNAME = "X-SKYPE-USERNAME";
    // Properties only ContactsStruct has. We also use this.
    public static readonly PROPERTY_X_QQ = "X-QQ";
    public static readonly PROPERTY_X_NETMEETING = "X-NETMEETING";

    // Phone number for Skype, available as usual phone.
    public static readonly PROPERTY_X_SKYPE_PSTNNUMBER = "X-SKYPE-PSTNNUMBER";

    // Property for specific fields.
    public static readonly PROPERTY_X_SYSTEM_CUSTOM = "X-ANDROID-CUSTOM";

    // Properties for DoCoMo vCard.
    public static readonly PROPERTY_X_CLASS = "X-CLASS";
    public static readonly PROPERTY_X_REDUCTION = "X-REDUCTION";
    public static readonly PROPERTY_X_NO = "X-NO";
    public static readonly PROPERTY_X_DCM_HMN_MODE = "X-DCM-HMN-MODE";

    public static readonly PARAM_TYPE = "TYPE";

    public static readonly PARAM_TYPE_HOME = "HOME";
    public static readonly PARAM_TYPE_WORK = "WORK";
    public static readonly PARAM_TYPE_FAX = "FAX";
    public static readonly PARAM_TYPE_CELL = "CELL";
    public static readonly PARAM_TYPE_VOICE = "VOICE";
    public static readonly PARAM_TYPE_OTHER = "OTHER";
    public static readonly PARAM_TYPE_INTERNET = "INTERNET";

    public static readonly PARAM_VALUE = "VALUE";
    public static readonly PARAM_CHARSET = "CHARSET";
    public static readonly PARAM_ENCODING = "ENCODING";

    // Abbreviation of "preferred" according to vCard 2.1 specification.
    // We interpret this value as "primary" property during import/export.
    //
    // Note: Both vCard specs does not mention anything about the requirement for this parameter,
    //       but there may be some vCard importer which will get confused with more than
    //       one "PREF"s in one property name, while system accepts them.
    public static readonly PARAM_TYPE_PREF = "PREF";

    // Phone type parameters valid in vCard and known to ContactsContract, but not so common.
    public static readonly PARAM_TYPE_CAR = "CAR";
    public static readonly PARAM_TYPE_ISDN = "ISDN";
    public static readonly PARAM_TYPE_PAGER = "PAGER";
    public static readonly PARAM_TYPE_TLX = "TLX";  // Telex

    // Phone types existing in vCard 2.1 but not known to ContactsContract.
    public static readonly PARAM_TYPE_MODEM = "MODEM";
    public static readonly PARAM_TYPE_MSG = "MSG";
    public static readonly PARAM_TYPE_BBS = "BBS";
    public static readonly PARAM_TYPE_VIDEO = "VIDEO";

    public static readonly PARAM_ENCODING_7BIT:string = "7BIT";
    public static readonly PARAM_ENCODING_8BIT = "8BIT";
    public static readonly PARAM_ENCODING_QP = "QUOTED-PRINTABLE";
    public static readonly PARAM_ENCODING_BASE64 = "BASE64";  // Available in vCard 2.1
    public static readonly PARAM_ENCODING_B = "B";  // Available in vCard 3.0

    // TYPE parameters for Phones, which are not formally valid in vCard (at least 2.1).
    // These types are basically encoded to "X-" parameters when composing vCard.
    // Parser passes these when "X-" is added to the parameter or not.
    public static readonly PARAM_PHONE_EXTRA_TYPE_CALLBACK = "CALLBACK";
    public static readonly PARAM_PHONE_EXTRA_TYPE_RADIO = "RADIO";
    public static readonly PARAM_PHONE_EXTRA_TYPE_TTY_TDD = "TTY-TDD";
    public static readonly PARAM_PHONE_EXTRA_TYPE_ASSISTANT = "ASSISTANT";
    // vCard composer translates this type to "WORK" + "PREF". Just for parsing.
    public static readonly PARAM_PHONE_EXTRA_TYPE_COMPANY_MAIN = "COMPANY-MAIN";
    // vCard composer translates this type to "VOICE" Just for parsing.
    public static readonly PARAM_PHONE_EXTRA_TYPE_OTHER = "OTHER";

    // TYPE parameters for postal addresses.
    public static readonly PARAM_ADR_TYPE_PARCEL = "PARCEL";
    public static readonly PARAM_ADR_TYPE_DOM = "DOM";
    public static readonly PARAM_ADR_TYPE_INTL = "INTL";
    // {@link VCardBuilder} translates this type to "X-OTHER".
    public static readonly PARAM_ADR_EXTRA_TYPE_OTHER = "OTHER";

    public static readonly PARAM_LANGUAGE = "LANGUAGE";

    // SORT-AS parameter introduced in vCard 4.0 (as of rev.13)
    public static readonly PARAM_SORT_AS = "SORT-AS";

    // TYPE parameters not officially valid but used in some vCard exporter.
    // Do not use in composer side.
    public static readonly PARAM_EXTRA_TYPE_COMPANY = "COMPANY";

    //// Mainly for package readonlyants.

    // DoCoMo specific type parameter. Used with "SOUND" property, which is alternate of
    // SORT- invCard 3.0.
    /* package */ static readonly PARAM_TYPE_X_IRMC_N = "X-IRMC-N";

    // Used in unit test.
    public static readonly MAX_DATA_COLUMN = 15;

    /* package */ static readonly MAX_CHARACTER_NUMS_QP = 76;
    static readonly MAX_CHARACTER_NUMS_BASE64_V30 = 75;
}

export class ImportOnly {
    static readonly PROPERTY_X_NICKNAME = "X-NICKNAME";
}